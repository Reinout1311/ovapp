import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

public class GUIprofile implements ActionListener {

    JFrame f = new JFrame("OVapp");
//    HashMap<String, String> Profileinfo = new HashMap<String, String>();

    JButton profileBack = new JButton();
    JLabel userNameField = new JLabel();                // input userName
    JLabel userAchternameField  = new JLabel();  //input userAchtername
    JLabel userAdresField = new JLabel();  //input
    JLabel userTelephoneNumberField = new JLabel();
    JLabel name = new JLabel("naam:");
    JLabel achtername = new JLabel("Achternaam:");
    JLabel adres = new JLabel("adres:");
    JLabel telephoneNumber = new JLabel("telefoonnummer:");
    JLabel messageLabel = new JLabel();
    JButton changeSettings = new JButton("verander gegevens");
    JButton savedRoutes = new JButton("opgeslagen routes");

    //panel voor profiel

    GUIprofile(){

//        Profileinfo = GUIprofileOriginal;
        name.setBounds(50,100,75,25);
        achtername.setBounds(50,150,75,25);
        adres.setBounds(150,250,250,35);
        telephoneNumber.setBounds(250,350,300,45);
        messageLabel.setBounds(300,350,350,55);
        messageLabel.setFont(new Font(null,Font.ITALIC, 20));

        userNameField.setBounds(125,100,200,25);
        userAchternameField.setBounds(125,150,200,25);
        userAdresField.setBounds(125,200,200,25);
        userTelephoneNumberField.setBounds(125,250,200,25);

        profileBack.setBounds(125, 200, 100, 25);
        profileBack.setFocusable(false);
        profileBack.addActionListener(this);

        changeSettings.setBounds(225, 200, 100, 25);
        changeSettings.setFocusable(false);
        changeSettings.addActionListener(this);

        savedRoutes.setBounds(325, 200, 100, 25);
        savedRoutes.setFocusable(false);
        savedRoutes.addActionListener(this);


        f.add(userNameField);
        f.add(userAchternameField);
        f.add(userAdresField);
        f.add(userTelephoneNumberField);
        f.add(profileBack);
        f.add(changeSettings);
        f.add(savedRoutes);
        f.setSize(420, 420);
        f.setVisible(true);
        f.setLayout(null);
//        f.setLayout(null);
    }

    @Override
    public void actionPerformed(ActionEvent e) {


    }
}
