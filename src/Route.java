import java.time.LocalTime;
import java.util.ArrayList;

public class Route {

    public ArrayList<Stop> stops = new ArrayList<>();


    Route(Location location, LocalTime departure) {

        var stop = new Stop(location.getName(), LocalTime.MAX, departure);
        stops.add(stop);

    }



    public void addStop(Location location, LocalTime arrival, LocalTime departure) {

        var stop = new Stop(location.getName(), arrival, departure);
        stops.add(stop);

    }


//    public void addStart(Location location, LocalTime departure) {
//
//        var stop = new Stop(location.getName(), LocalTime.MAX, departure);
//        stops.add(stop);
//
//    }

    public void addEnd(Location location, LocalTime arrival) {

        var stop = new Stop(location.getName(), arrival, LocalTime.MIN);
        stops.add(stop);

    }



    public String getKey() {

        String key = stops.get(0).getName();

        for (int i = 1; i < stops.size(); i++) {
            key += "-";
            key += stops.get(i).getName();
        }
        return key;

    }



    public LocalTime getDeparture() { return stops.get(0).getDeparture(); }


//    public LocalTime getDeparture( Location location ) {
//
//        for (var e : stops) {
//            if (e.getName().equals(location.getName())) {
//                return e.getDeparture();
//            }
//        }
//        return null;
//    }



    public LocalTime getArrival(){ return stops.get(stops.size() - 1).getArrival(); }

}
