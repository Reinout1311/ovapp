import java.util.Locale;
import java.util.ResourceBundle;

public class Language {

   private String languageNL = "nl";
   private String languageEN = "en";
   private String countryNL="NL";
   private String countryEN= "US";
   private ResourceBundle resource = ResourceBundle.getBundle("OVappBundle" , Locale.getDefault());

    public String getString(String key){

        return this.resource.getString(key);

    }

   public void setLanguage(int languageChoice){

        switch (languageChoice){
            case 0:
                break;
            case 1:
                var l = new Locale(languageNL, countryNL);
                 this.resource = ResourceBundle.getBundle("OVappBundle", l );
                break;
            case 2:
                var p = new Locale(languageEN,countryEN);
                this.resource = ResourceBundle.getBundle("OVappBundle", p);
                break;
            default:
                break;
        }

    }

}
