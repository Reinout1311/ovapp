import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.time.LocalTime;
import java.util.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.awt.image.BufferedImage;

public class GUIMainMenu {

    Language language = new Language();
    Data data = new Data();
    HashMap logininfo = new HashMap<String, String>();

    //kaart
    JFrame map = new JFrame("Map");
    JPanel mapMap = new JPanel();


    //Main window & panels
    JFrame frame = new JFrame("OV app");

    //    JPanel mainMenu = new JPanel();
    JPanel profile = new JPanel();
    JPanel routesList = new JPanel();
    JPanel routeinfo = new JPanel();
    JPanel loginScreen = new JPanel();
    JPanel aboveBar = new JPanel();
    JPanel register = new JPanel();
    JPanel chooseroute = new JPanel();
    FlowLayout flowLayout = new FlowLayout();

    //voor routeinfo
    JTextArea stationInfoAlkmaar = new JTextArea();
    JTextArea stationInfoGroningen = new JTextArea();
    JTextArea stationInfoRotterdam = new JTextArea();
    JTextArea stationInfoMaastricht = new JTextArea();
    JTextArea stationInfoApeldoorn = new JTextArea();
    JTextArea stationInfoUtrecht = new JTextArea();
    JTextArea price1 = new JTextArea();
    JTextArea price2 = new JTextArea();
    JTextArea price3 = new JTextArea();
    JTextArea price4 = new JTextArea();
    JTextArea price5 = new JTextArea();
    JTextArea km1 = new JTextArea();
    JTextArea km2 = new JTextArea();
    JTextArea km3 = new JTextArea();
    JTextArea km4 = new JTextArea();
    JTextArea km5 = new JTextArea();

    //de buttons bij de routelijst
    JButton routelistbutton1 = new JButton();
    JButton routelistbutton2 = new JButton();
    JButton routelistbutton3 = new JButton();
    JButton routelistbutton4 = new JButton();

    // voor login
    JButton loginButton = new JButton("Login");
    JButton registerButton = new JButton("Registreer");
    JTextField userIDField = new JTextField();                // input userid
    JPasswordField userPasswordField = new JPasswordField();  // input password
    JLabel useridLabel = new JLabel("E-mail:");
    JLabel userPasswordLabel = new JLabel("Wachtwoord:");
    JLabel messageLabel = new JLabel();
    JButton homeButton = new JButton();

    //Register
    JButton registerAccept = new JButton("Opslaan");
    JTextField registerFirstnameField = new JTextField();
    JLabel registerFirstname = new JLabel("Naam:");
    JTextField registerLastnameField = new JTextField();
    JLabel registerLastname = new JLabel("Achternaam:");
    JTextField registerEmailField = new JTextField();
    JLabel registerEmail = new JLabel("E-mail:");
    JTextField registerPasswordField = new JTextField();
    JLabel registerPassword = new JLabel("Wachtwoord:");
    JTextField registerPasswordRepeatField = new JTextField();
    JLabel registerPasswordRepeat = new JLabel("Wachtwoord:");
    JTextField registerAddressField = new JTextField();
    JLabel registerAddress = new JLabel("Address");

    // voor profiel
    JLabel name = new JLabel("Naam:");
    JTextField userNameField = new JTextField();
    JLabel achtername = new JLabel("Achternaam:");
    JTextField userAchternameField =new JTextField();
    JLabel adres = new JLabel("Adres:");
    JTextField userAdresField = new JTextField();
    JLabel telephoneNumber = new JLabel("Telefoonnummer:");
    JTextField userTelephoneNumberField = new JTextField();
    JLabel messageLabel2 = new JLabel();
    JButton changeSettings = new JButton("verander gegevens");
    JButton savedRoutes = new JButton("opgeslagen routes");

    //maakt een array aan die in een combobox komt te staan
    private String[] locationsTrain = {"Rotterdam","Groningen","Maastricht","Utrecht","Alkmaar", "Apeldoorn"}; //maakt ook een array aan voor in de combobox
    private String[] locationsPlane = {"Amsterdam","Londen","Parijs","Berlijn","Praag","Moskou","Rome","Madrid","Lissabon","Oslo","Helsinki","Kopenhagen","Athene","Boedapest","Wenen","Brussel","Geneve","Istanboel","Boedapest","Dublin","Stockholm","Warschau","Reykjavik",}; //maakt ook een array aan voor in de combobox

    //maakt de combobox en voegt de array er aan toe
    JComboBox firstLocationTrain = new JComboBox(locationsTrain);
    JComboBox secondLocationTrain = new JComboBox(locationsTrain);
    JComboBox firstLocationPlane = new JComboBox(locationsPlane);
    JComboBox secondLocationPlane = new JComboBox(locationsPlane);

    //Vullen combobox tijdkeuze
    String[] times = {"0","1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23",};
    JComboBox timeChoice = new JComboBox(times);

    String[] minutes = {"0","1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59"};
    JComboBox minuteChoice = new JComboBox(minutes);

    String[] routes = {"hey","hoi","doei","gegroet"};
    JComboBox routesComboBox = new JComboBox(routes);

    //maak jlist aan
    private JList routelistGui = new JList();

    //Aanmaken buttons & labels
    JButton confirmTrip = new JButton(language.getString("bereken_route"));
    JButton login = new JButton("Log in");
    JLabel labelLocationA = new JLabel();
    JLabel labelLocationB = new JLabel();
    JLabel labelVehicles = new JLabel();

    JLabel labelHourChoice = new JLabel();
    JLabel labelMinuteChoice = new JLabel();
    JLabel welcomeMessage = new JLabel();

    // button voor de map
    JButton mapOpen = new JButton();

    // buttons voor het switchen naar engels & naar nl
    JButton switchLanguageToEnglish = new JButton();
    JButton switchLanguageToDutch = new JButton();

    JLabel planeIcon = new JLabel();
    JLabel trainIcon = new JLabel();
    JLabel alkmaarMaastricht = new JLabel();
    JLabel alkmaarUtrecht = new JLabel();
    JLabel rotterdamGroningen = new JLabel();
    JLabel rotterdamMaastricht = new JLabel();
    JLabel utrechtApeldoorn = new JLabel();


    //maakt de RadioButton en vult deze
    JButton vehicleChoice1 = new JButton(language.getString("vliegtuig"));
    JButton vehicleChoice2 = new JButton(language.getString("trein"));

    BufferedImage icon;

    GUIMainMenu() throws IOException {
        String filepath = "src/images/OVapp Icon(2).jpg";
        File file = new File(filepath);
        icon = ImageIO.read(file);
        frame.setIconImage(icon);

        planeIcon.setIcon((new javax.swing.ImageIcon(getClass().getResource("Plaatjes/PlaneOv.png"))));
        trainIcon.setIcon((new javax.swing.ImageIcon(getClass().getResource("Plaatjes/TrainOv.png"))));
        utrechtApeldoorn.setIcon((new javax.swing.ImageIcon(getClass().getResource("Plaatjes/Utrecht-Apeldoorn.png"))));
        alkmaarMaastricht.setIcon((new javax.swing.ImageIcon(getClass().getResource("Plaatjes/Alkmaar-Maastricht.png"))));
        alkmaarUtrecht.setIcon((new javax.swing.ImageIcon(getClass().getResource("Plaatjes/Alkmaar-Utrecht-Apeldoorn-Utrecht.png"))));
        rotterdamMaastricht.setIcon((new javax.swing.ImageIcon(getClass().getResource("Plaatjes/Rotterdam-Maastricht.png"))));
        rotterdamGroningen.setIcon((new javax.swing.ImageIcon(getClass().getResource("Plaatjes/Rotterdam-Groningen.png"))));

        vehicleChoice1.setText("Vliegtuig");
        vehicleChoice2.setText("Trein");

        ButtonGroup vehicleButtons = new ButtonGroup();
        vehicleButtons.add(vehicleChoice1);
        vehicleButtons.add(vehicleChoice2);
        trainIcon.setVisible(true);
        planeIcon.setVisible(false);
        chooseroute.add(trainIcon);
        chooseroute.add(planeIcon);

        vehicleChoice1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                trainIcon.setVisible(false);
                planeIcon.setVisible(true);
                firstLocationTrain.setVisible(false);
                secondLocationTrain.setVisible(false);
                firstLocationPlane.setVisible(true);
                secondLocationPlane.setVisible(true);
            }
        });

        vehicleChoice2.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                planeIcon.setVisible(false);
                trainIcon.setVisible(true);
                firstLocationTrain.setVisible(true);
                secondLocationTrain.setVisible(true);
                firstLocationPlane.setVisible(false);
                secondLocationPlane.setVisible(false);
            }
        });


        //zorgt ervoor dat je hetgene wat je zoekt in de combobox ook in kan typen
        firstLocationTrain.setEditable(false);
        secondLocationTrain.setEditable(false);
        firstLocationPlane.setEditable(false);
        secondLocationPlane.setEditable(false);

        var p = getClass().getResource("Plaatjes/VlagEngelsklein.png");
        switchLanguageToEnglish.setIcon(new javax.swing.ImageIcon(p));

        var q = getClass().getResource("Plaatjes/VlagNederlandsklein.png");
        switchLanguageToDutch.setIcon(new javax.swing.ImageIcon(q));

        labelLocationA.setText(language.getString("startlocatie"));
        labelLocationB.setText(language.getString("eindbestemming"));
        labelVehicles.setText(language.getString("vervoersmiddel"));

        labelHourChoice.setText(language.getString("uur"));
        labelMinuteChoice.setText("Min");
//        welcomeMessage.setText(language.getString("welkom"));
        welcomeMessage.setFont(new Font("Serif", Font.PLAIN, 20));

        welcomeMessage.setForeground(Color.red);

        firstLocationTrain.setBounds(300, 60, 350, 35);
        secondLocationTrain.setBounds(750, 60, 350, 35);
        firstLocationPlane.setBounds(300, 60, 350, 35);
        secondLocationPlane.setBounds(750, 60, 350, 35);
        vehicleChoice1.setBounds(50, 50, 300, 30);
        vehicleChoice1.setSize(150,30);
        vehicleChoice2.setBounds(50,110,300,30);
        vehicleChoice2.setSize(150,30);
        planeIcon.setBounds(210,70,50,50);
        trainIcon.setBounds(210, 70, 50, 50);

        timeChoice.setBounds(350,130,50,50);
        minuteChoice.setBounds(450, 130, 100, 50);
        minuteChoice.setSize(50, 50);
        welcomeMessage.setBounds(520, 115, 600, 50);
        labelLocationA.setBounds(300, 20, 200, 50);
        labelLocationB.setBounds(750, 20, 200, 50);
        labelVehicles.setBounds(75, 25, 200, 15);

        labelHourChoice.setBounds(325, 140, 200, 30);
        labelMinuteChoice.setBounds(425, 140, 200, 30);
        confirmTrip.setBounds(925, 125, 150, 50);

        switchLanguageToEnglish.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("testen");

                language.setLanguage(2);

                // TODO Method maken in language die alle Strings refreshed

                labelLocationA.setText(language.getString("startlocatie"));
                labelLocationB.setText(language.getString("eindbestemming"));
                labelVehicles.setText(language.getString("vervoersmiddel"));
                labelHourChoice.setText(language.getString("uur"));
                welcomeMessage.setBounds(165, 100, 600, 50);
                vehicleChoice1.setText(language.getString("vliegtuig"));
                vehicleChoice2.setText(language.getString("trein"));
                confirmTrip.setText(language.getString("bereken_route"));
                mapOpen.setText(language.getString("kaart"));
            }
        });

        switchLanguageToDutch.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                System.out.println("testnl");
                language.setLanguage(1);

                labelLocationA.setText(language.getString("startlocatie"));
                labelLocationB.setText(language.getString("eindbestemming"));
                labelVehicles.setText(language.getString("vervoersmiddel"));

                labelHourChoice.setText(language.getString("uur"));
//                welcomeMessage.setText(language.getString("welkom"));
                welcomeMessage.setBounds(225, 100, 600, 50);
                vehicleChoice1.setText(language.getString("vliegtuig"));
                vehicleChoice2.setText(language.getString("trein"));
                confirmTrip.setText(language.getString("bereken_route"));
                mapOpen.setText(language.getString("kaart"));
            }
        });


        login.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                chooseroute.setVisible(false);
                routesList.setVisible(false);
//                mainMenu.setVisible(false);
                login.setVisible(false);
                homeButton.setVisible(true);
                loginScreen.setVisible(true);
                routeinfo.setVisible(false);
            }
        });

        confirmTrip.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                LocalTime chosenTime = LocalTime.of(timeChoice.getSelectedIndex(),minuteChoice.getSelectedIndex());
                String locationA = firstLocationTrain.getSelectedItem().toString();
                String locationB = secondLocationTrain.getSelectedItem().toString();

                var b = data.searchRoutes(locationA,locationB,chosenTime);


                Set<Map.Entry<String, Integer> > entrySet = b.entrySet();

                // Creating an ArrayList of Entry objects
                ArrayList<Map.Entry<String, Integer> > arraySet = new ArrayList<>(entrySet);

                DefaultListModel<String> listModel = new DefaultListModel<>();

                for (var p : arraySet){
                    listModel.addElement(p.toString());
                }

                routelistGui.setModel(listModel);
                routesList.setVisible(true);
                routeinfo.setVisible(true);
            }
        });


        //het frame en main menu panel word ingesteld
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Exit out of application
        frame.setResizable(false); // prevent frame from being resized
        frame.setVisible(true);
        frame.setSize(1280, 720);

        mapOpen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                map.setVisible(true);
            }
        });

        //panel voor de route invoeren
        frame.add(chooseroute);
        chooseroute.setBounds(0, 50, 1280, 200);
        chooseroute.setVisible(true);
        chooseroute.setLayout(null);
        chooseroute.setBackground(new Color(134269874));

        chooseroute.add(confirmTrip);
        chooseroute.add(labelLocationA);
        chooseroute.add(labelLocationB);
        chooseroute.add(labelVehicles);
        chooseroute.add(vehicleChoice1);
        chooseroute.add(vehicleChoice2);
        chooseroute.add(timeChoice);
        chooseroute.add(minuteChoice);
        chooseroute.add(labelHourChoice);
        chooseroute.add(labelMinuteChoice);
        chooseroute.add(welcomeMessage);
        chooseroute.add(firstLocationTrain);
        chooseroute.add(secondLocationTrain);
        chooseroute.add(firstLocationPlane);
        chooseroute.add(secondLocationPlane);

        //panel voor profiel
        frame.add(profile);
        profile.setBounds(0, 75, 1280, 670);
        profile.setVisible(false);
        profile.setLayout(null);

        profile.add(name);
        profile.add(userNameField);
        profile.add(achtername);
        profile.add(userAchternameField);
        profile.add(adres);
        profile.add(userAdresField);
        profile.add(telephoneNumber);
        profile.add(userTelephoneNumberField);
        profile.add(changeSettings);
        profile.add(savedRoutes);
        frame.add(profile);

        name.setBounds(500,100,75,25);
        userNameField.setBounds(600,100,200,25);
        userNameField.setBorder(BorderFactory.createLineBorder(Color.black));
        achtername.setBounds(500,150,75,25);
        userAchternameField.setBounds(600,150,200,25);
        userAchternameField.setBorder(BorderFactory.createLineBorder(Color.black));
        adres.setBounds(500,200,200,35);
        userAdresField.setBounds(600,200,200,25);
        userAdresField.setBorder(BorderFactory.createLineBorder(Color.black));
        telephoneNumber.setBounds(500,230,300,45);
        userTelephoneNumberField.setBounds(620,240,200,25);
        userTelephoneNumberField.setBorder(BorderFactory.createLineBorder(Color.black));
        messageLabel.setBounds(650,450,350,55);
        messageLabel.setFont(new Font((String)null,2,20));
        changeSettings.setBounds(750,350,100,25);
        changeSettings.setFocusable(false);
        savedRoutes.setBounds(900,350,100,25);
        savedRoutes.setFocusable(false);


        //Register
        registerAccept.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == registerAccept) {
                    String newEmail = registerEmailField.getText();
                    String newPassword = registerPasswordField.getText();
                    String newFirstname = registerFirstname.getText();
                    String newLastname = registerLastname.getText();
                    String newAddress = registerAddress.getText();

                    data.addUserJson(newFirstname,newLastname,newAddress,newEmail,newPassword,new ArrayList<Route>());
                    data.addCredential(newEmail,newPassword);
                }
            }
        });


        register.add(registerFirstname);
        registerFirstnameField.setSize(20, 10);
        register.add(registerFirstnameField);
        register.add(registerAddress);
        register.add(registerAddressField);
        register.add(registerLastname);
        register.add(registerLastnameField);
        register.add(registerEmail);
        register.add(registerEmailField);
        register.add(registerPassword);
        register.add(registerPasswordField);
        register.add(registerPasswordRepeat);
        register.add(registerPasswordRepeatField);
        register.add(registerAccept);

        registerFirstname.setBounds(500,100,75,25);
        registerFirstnameField.setBounds(600,100,200,25);
        registerFirstnameField.setBorder(BorderFactory.createLineBorder(Color.black));

        registerLastname.setBounds(500,150,75,25);
        registerLastnameField.setBounds(600,150,200,25);
        registerLastnameField.setBorder(BorderFactory.createLineBorder(Color.black));

        registerAddress.setBounds(500,200,75,25);
        registerAddressField.setBounds(600,200,200,25);
        registerAddressField.setBorder(BorderFactory.createLineBorder(Color.black));

        registerEmail.setBounds(500,250,75,25);
        registerEmailField.setBounds(600,250,200,25);
        registerEmailField.setBorder(BorderFactory.createLineBorder(Color.black));

        registerPassword.setBounds(500,300,75,25);
        registerPasswordField.setBounds(600,300,200,25);
        registerPasswordField.setBorder(BorderFactory.createLineBorder(Color.black));

        registerPasswordRepeat.setBounds(500,300,75,25);
        registerPasswordRepeatField.setBounds(600,300,200,25);
        registerPasswordRepeatField.setBorder(BorderFactory.createLineBorder(Color.black));
        registerAccept.setBounds(500,400,300,25);

        //register.setLayout(new GridLayout());

        register.setBounds(0, 75, 1280, 670);
        register.setVisible(false);
        register.setLayout(null);
        frame.add(register);

        //voor de balk bovenaan
        aboveBar.setLayout(null);
        aboveBar.setBounds(0,0,1280,50);
        login.setBounds(70, 7, 150, 35);
        switchLanguageToDutch.setBounds(1115, 5, 75, 40);
        switchLanguageToEnglish.setBounds(1190, 5, 75, 40);
        homeButton.setBounds(15,5,40,40);
        aboveBar.add(switchLanguageToDutch);
        aboveBar.add(switchLanguageToEnglish);
        aboveBar.add(login);

        aboveBar.setBackground(Color.lightGray);
        aboveBar.setVisible(true);
        aboveBar.add(homeButton);
        var g = getClass().getResource("Plaatjes/355795-huis-icon-gratis-vector (1).jpg");
        homeButton.setIcon(new javax.swing.ImageIcon(g));
        homeButton.setVisible(true);

        frame.add(aboveBar);

        homeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
//                mainMenu.setVisible(true);
                chooseroute.setVisible(true);
                loginScreen.setVisible(false);
                routesList.setVisible(false);
                login.setVisible(true);
                register.setVisible(false);
                routeinfo.setVisible(false);
                profile.setVisible(false);
            }
        });

        routelistGui.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                routeinfo.setVisible(true);
                utrechtApeldoorn.setVisible(true);
                price1.setVisible(true);
                km1.setVisible(true);
                stationInfoUtrecht.setVisible(true);
                stationInfoApeldoorn.setVisible(true);
                stationInfoRotterdam.setVisible(false);
                stationInfoMaastricht.setVisible(false);
                stationInfoAlkmaar.setVisible(false);
                alkmaarUtrecht.setVisible(false);
                alkmaarMaastricht.setVisible(false);
                rotterdamMaastricht.setVisible(false);
                price4.setVisible(false);
                price2.setVisible(false);
                price3.setVisible(false);
                km4.setVisible(false);
                km2.setVisible(false);
                km3.setVisible(false);
            }
        });


        routelistbutton2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                routeinfo.setVisible(true);
                alkmaarMaastricht.setVisible(true);
                price2.setVisible(true);
                km2.setVisible(true);
                stationInfoAlkmaar.setVisible(true);
                stationInfoMaastricht.setVisible(true);
                stationInfoUtrecht.setVisible(false);
                stationInfoRotterdam.setVisible(false);
                stationInfoApeldoorn.setVisible(false);
                alkmaarUtrecht.setVisible(false);
                rotterdamMaastricht.setVisible(false);
                utrechtApeldoorn.setVisible(false);
                price1.setVisible(false);
                price4.setVisible(false);
                price3.setVisible(false);
                km1.setVisible(false);
                km4.setVisible(false);
                km3.setVisible(false);
            }
        });

        routelistbutton3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                routeinfo.setVisible(true);
                alkmaarUtrecht.setVisible(true);
                price3.setVisible(true);
                km3.setVisible(true);
                stationInfoAlkmaar.setVisible(true);
                stationInfoUtrecht.setVisible(true);
                stationInfoMaastricht.setVisible(false);
                stationInfoRotterdam.setVisible(false);
                stationInfoApeldoorn.setVisible(false);
                alkmaarMaastricht.setVisible(false);
                rotterdamMaastricht.setVisible(false);
                utrechtApeldoorn.setVisible(false);
                price1.setVisible(false);
                price2.setVisible(false);
                price4.setVisible(false);
                km1.setVisible(false);
                km2.setVisible(false);
                km4.setVisible(false);
            }
        });

        routelistbutton4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                routeinfo.setVisible(true);
                rotterdamMaastricht.setVisible(true);
                price4.setVisible(true);
                km4.setVisible(true);
                stationInfoMaastricht.setVisible(true);
                stationInfoRotterdam.setVisible(true);
                stationInfoUtrecht.setVisible(false);
                stationInfoAlkmaar.setVisible(false);
                stationInfoApeldoorn.setVisible(false);
                alkmaarUtrecht.setVisible(false);
                alkmaarMaastricht.setVisible(false);
                utrechtApeldoorn.setVisible(false);
                price1.setVisible(false);
                price2.setVisible(false);
                price3.setVisible(false);
                km1.setVisible(false);
                km2.setVisible(false);
                km3.setVisible(false);
            }
        });

        //panel voor routelijst
        //TODO routeslist invullen
        frame.add(routesList);
        routesList.setBounds(0,250,640,470);
        routesList.setVisible(false);
        routesList.setLayout(new GridLayout());
        routelistGui.setSize(640,70);
        routesList.setBackground(Color.darkGray);

       //routelistGui.setBounds(0,0,640,370);
        routelistGui.setVisible(true);
        routelistGui.setVisibleRowCount(5);
        routelistGui.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JScrollPane scrollPane = new JScrollPane(routelistGui, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        routelistGui.setFixedCellHeight(100);
        routesList.add(scrollPane);

        //panel voor routeinformatie
        frame.add(routeinfo);
        routeinfo.add(mapOpen);
        routeinfo.add(stationInfoApeldoorn);
        routeinfo.add(stationInfoRotterdam);
        routeinfo.add(stationInfoGroningen);
        routeinfo.add(stationInfoMaastricht);
        routeinfo.add(stationInfoAlkmaar);
        routeinfo.add(stationInfoUtrecht);
        routeinfo.add(price1);
        routeinfo.add(price2);
        routeinfo.add(price3);
        routeinfo.add(price4);
        routeinfo.add(price5);
        routeinfo.add(km1);
        routeinfo.add(km2);
        routeinfo.add(km3);
        routeinfo.add(km4);
        routeinfo.add(km5);
        routeinfo.setLayout(null);
        mapOpen.setBounds(250,370, 150, 50);
        mapOpen.setText("Kaart");
        stationInfoApeldoorn.setText("Station Apeldoorn: \n" + "2 treinsporen\nen een OV-paal,\ngeen andere \nfaciliteiten.");
        stationInfoApeldoorn.setBounds(370, 40, 150, 100);
        stationInfoApeldoorn.setEditable(false);
        stationInfoApeldoorn.setVisible(false);
        stationInfoRotterdam.setText("Station Rotterdam: \n" + "8 treinsporen\nmet OV-poorten\nen meerdere faciliteiten.");
        stationInfoRotterdam.setBounds(370, 40, 150, 100);
        stationInfoRotterdam.setVisible(false);
        stationInfoRotterdam.setEditable(false);
        stationInfoGroningen.setText("Station Groningen: \n" + "4 treinsporen\nmet OV-palen\nen geen andere \nfaciliteiten.");
        stationInfoGroningen.setBounds(370, 40, 150, 100);
        stationInfoGroningen.setVisible(false);
        stationInfoGroningen.setEditable(false);
        stationInfoMaastricht.setText("Station Maastricht: \n" + "6 treinsporen\nmet OV-poorten\nen meerdere faciliteiten.");
        stationInfoMaastricht.setBounds(130, 40, 150, 100);
        stationInfoMaastricht.setVisible(false);
        stationInfoMaastricht.setEditable(false);
        stationInfoAlkmaar.setText("Station Alkmaar: \n" + "4 treinsporen\nmet OV-palen\nen geen andere \nfaciliteiten.");
        stationInfoAlkmaar.setBounds(370, 40, 150, 100);
        stationInfoAlkmaar.setVisible(false);
        stationInfoAlkmaar.setEditable(false);
        stationInfoUtrecht.setText("Station Utrecht: \n" + "6 treinsporen\nmet OV-poorten\nen meerdere faciliteiten.");
        stationInfoUtrecht.setBounds(130, 40, 150, 100);
        stationInfoUtrecht.setVisible(false);
        stationInfoUtrecht.setEditable(false);
        price1.setText("Kosten: 5,95 euro");
        price1.setBounds(130, 250, 125, 25);
        price1.setVisible(false);
        price1.setEditable(false);
        price2.setText("Kosten: 11,00 euro");
        price2.setBounds(130, 250, 125, 25);
        price2.setVisible(false);
        price2.setEditable(false);
        price3.setText("Kosten: 4,95 euro");
        price3.setBounds(130, 250, 125, 25);
        price3.setVisible(false);
        price3.setEditable(false);
        price4.setText("Kosten: 9,95 euro");
        price4.setBounds(130, 250, 125, 25);
        price4.setVisible(false);
        price4.setEditable(false);
        price5.setText("Kosten: 8,95 euro");
        price5.setBounds(130, 250, 125, 25);
        price5.setVisible(false);
        price5.setEditable(false);
        km1.setText("Kilometers: 92,4");
        km1.setBounds(370, 250, 125, 25);
        km1.setVisible(false);
        km1.setEditable(false);
        km2.setText("Kilometers: 254,3");
        km2.setBounds(370, 250, 125, 25);
        km2.setVisible(false);
        km2.setEditable(false);
        km3.setText("Kilometers: 86,8");
        km3.setBounds(370, 250, 125, 25);
        km3.setVisible(false);
        km3.setEditable(false);
        km4.setText("Kilometers: 198,0");
        km4.setBounds(370, 250, 125, 25);
        km4.setVisible(false);
        km4.setEditable(false);
        km5.setText("Kilometers: 245,9");
        km5.setBounds(370, 250, 125, 25);
        km5.setVisible(false);
        km5.setEditable(false);
        routeinfo.setVisible(false);
        routeinfo.setBounds(640,250,640,470);
        routeinfo.setBackground(new Color(891531234));

        //panel voor inlog
        useridLabel.setBounds(445, 200, 85, 35);
        userPasswordLabel.setBounds(445, 260, 85, 35);
        messageLabel.setBounds(525, 300, 250, 35);
        messageLabel.setFont(new Font(null, Font.ITALIC, 20));

        userIDField.setBounds(525, 200, 250, 35);
        userPasswordField.setBounds(525, 260, 250, 35);

        loginButton.setBounds(475, 350, 150, 35);
        loginButton.setFocusable(false);

        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                logininfo = data.getLoginInfo();
                if (e.getSource() == loginButton) {

                    String userID = userIDField.getText();
                    String password = String.valueOf(userPasswordField.getPassword());

                    if (logininfo.containsKey(userID)) {
                        if (logininfo.get(userID).equals(password)) {
                            messageLabel.setForeground(Color.green);
                            messageLabel.setText("Login succesfull");

                            loginScreen.setVisible(false);
                            profile.setVisible(true);
                        }
                    } else {
                        messageLabel.setForeground(Color.red);
                        messageLabel.setText("Unknown E-mail/password");
                    }
                }
            }
        });

        registerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == registerButton) {

                    loginScreen.setVisible(false);
                    register.setVisible(true);
                }
            }
        });

        registerButton.setBounds(625, 350, 150, 35);
        registerButton.setFocusable(false);
        loginScreen.add(loginButton);
        loginScreen.add(registerButton);
        loginScreen.add(userIDField);
        loginScreen.add(userPasswordField);
        loginScreen.add(useridLabel);
        loginScreen.add(userPasswordLabel);
        loginScreen.add(messageLabel);

        loginScreen.setBounds(0, 50, 1280,680);
        loginScreen.setVisible(false);
        loginScreen.setLayout(null);
        frame.add(loginScreen);

        // maakt map frame aan
        mapMap.add(utrechtApeldoorn);
        mapMap.add(alkmaarMaastricht);
        mapMap.add(alkmaarUtrecht);
        mapMap.add(rotterdamMaastricht);
        mapMap.add(rotterdamGroningen);
        mapMap.setLayout(null);
        mapMap.setVisible(true);
        mapMap.setSize(1000,1000);
        mapMap.setBackground(new Color(891531234));
        utrechtApeldoorn.setBounds(150, 0, 700 ,700);
        utrechtApeldoorn.setVisible(false);
        alkmaarMaastricht.setBounds(150, 0, 700 ,700);
        alkmaarMaastricht.setVisible(false);
        alkmaarUtrecht.setBounds(150, 0, 700 ,700);
        alkmaarUtrecht.setVisible(false);
        rotterdamMaastricht.setBounds(150, 0, 700 ,700);
        rotterdamMaastricht.setVisible(false);
        rotterdamGroningen.setBounds(150, 0, 700 ,700);
        rotterdamGroningen.setVisible(false);
        map.add(mapMap);

        map.setResizable(false);
        map.setVisible(false);
        map.setLayout(null);
        map.setSize(1000,1000);
        frame.add(map);

}

}

