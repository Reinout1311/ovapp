import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.FileReader;
import java.io.FileWriter;
import java.time.LocalTime;
import java.util.*;

public class Data {

    public final HashMap<String, Location> locationMap = new HashMap<>();
    private final SortedMap<String, HashSet<Route>> routeMap = new TreeMap<>();

    public HashMap<String, String> logininfo = new HashMap<String, String>();

    Data() {

        logininfo.put("reinoutwitter@gmail.com","123");
        logininfo.put("test","000");
        logininfo.put("rutgervanschouwen@email.com","1234");

        Location location = new Location("Alkmaar");
        locationMap.put(location.getName(), location);

        location = new Location("Utrecht");
        locationMap.put(location.getName(), location);

        location = new Location("Apeldoorn");
        locationMap.put(location.getName(), location);

        location = new Location("Groningen");
        locationMap.put(location.getName(), location);

        location = new Location("Maastricht");
        locationMap.put(location.getName(), location);

        location = new Location("Rotterdam");
        locationMap.put(location.getName(), location);

        // Alkmaar - Utrecht - Apeldoorn - Groningen


        for (int hour = 7; hour <= 22; hour += 2) {
            Route route = new Route(locationMap.get("Alkmaar"), LocalTime.of(hour, 0));
            route.addStop(locationMap.get("Utrecht"), LocalTime.of(hour, 30), LocalTime.of(hour, 31));
            route.addStop(locationMap.get("Apeldoorn"), LocalTime.of(hour + 1, 0), LocalTime.of(hour + 1, 1));
            route.addEnd(locationMap.get("Groningen"), LocalTime.of(hour + 2, 0));
            addRoute(route);
        }


        // Groningen - Apeldoorn - Utrecht - Alkmaar
        {

            for (int hour = 7; hour <= 22; hour += 2) {
                Route route = new Route(locationMap.get("Groningen"), LocalTime.of(hour, 0));
                route.addStop(locationMap.get("Apeldoorn"), LocalTime.of(hour, 30), LocalTime.of(hour, 31));
                route.addStop(locationMap.get("Utrecht"), LocalTime.of(hour + 1, 0), LocalTime.of(hour + 1, 1));
                route.addEnd(locationMap.get("Alkmaar"), LocalTime.of(hour + 2, 0));
                addRoute(route);
            }
        }

        // Alkmaar - Utrecht - Apeldoorn - Maastricht
        {
            for (int hour = 7; hour <= 22; hour += 2) {
                Route route = new Route(locationMap.get("Alkmaar"), LocalTime.of(hour, 0));
                route.addStop(locationMap.get("Utrecht"), LocalTime.of(hour, 30), LocalTime.of(hour, 31));
                route.addStop(locationMap.get("Apeldoorn"), LocalTime.of(hour + 1, 0), LocalTime.of(hour + 1, 1));
                route.addEnd(locationMap.get("Maastricht"), LocalTime.of(hour + 2, 0));
                addRoute(route);
            }
        }

        // Maastricht - Apeldoorn - Utrecht - Alkmaar
        {
            for (int hour = 7; hour <= 22; hour += 2) {
                Route route = new Route(locationMap.get("Maastricht"), LocalTime.of(hour, 0));
                route.addStop(locationMap.get("Apeldoorn"), LocalTime.of(hour, 30), LocalTime.of(hour, 31));
                route.addStop(locationMap.get("Utrecht"), LocalTime.of(hour + 1, 0), LocalTime.of(hour + 1, 1));
                route.addEnd(locationMap.get("Alkmaar"), LocalTime.of(hour + 2, 0));
                addRoute(route);
            }
        }

        // Rotterdam – Utrecht – Apeldoorn – Maastricht
        {
            for (int hour = 7; hour <= 22; hour += 2) {
                Route route = new Route(locationMap.get("Rotterdam"), LocalTime.of(hour, 0));
                route.addStop(locationMap.get("Utrecht"), LocalTime.of(hour, 30), LocalTime.of(hour, 31));
                route.addStop(locationMap.get("Apeldoorn"), LocalTime.of(hour + 1, 0), LocalTime.of(hour + 1, 1));
                route.addEnd(locationMap.get("Maastricht"), LocalTime.of(hour + 2, 0));
                addRoute(route);
            }
        }

        // Maastricht – Apeldoorn – Utrecht – Rotterdam
        {
            for (int hour = 7; hour <= 22; hour += 2) {
                Route route = new Route(locationMap.get("Maastricht"), LocalTime.of(hour, 0));
                route.addStop(locationMap.get("Apeldoorn"), LocalTime.of(hour, 30), LocalTime.of(hour, 31));
                route.addStop(locationMap.get("Utrecht"), LocalTime.of(hour + 1, 0), LocalTime.of(hour + 1, 1));
                route.addEnd(locationMap.get("Rotterdam"), LocalTime.of(hour + 2, 0));
                addRoute(route);
            }
        }

        // Rotterdam – Utrecht – Apeldoorn – Groningen
        {
            for (int hour = 7; hour <= 22; hour += 2) {
                Route route = new Route(locationMap.get("Rotterdam"), LocalTime.of(hour, 0));
                route.addStop(locationMap.get("Utrecht"), LocalTime.of(hour, 30), LocalTime.of(hour, 31));
                route.addStop(locationMap.get("Apeldoorn"), LocalTime.of(hour + 1, 0), LocalTime.of(hour + 1, 1));
                route.addEnd(locationMap.get("Groningen"), LocalTime.of(hour + 2, 0));
                addRoute(route);
            }
        }

        // Groningen – Apeldoorn – Utrecht – Rotterdam
        {
            for (int hour = 7; hour <= 22; hour += 2) {
                Route route = new Route(locationMap.get("Groningen"), LocalTime.of(hour, 0));
                route.addStop(locationMap.get("Apeldoorn"), LocalTime.of(hour, 30), LocalTime.of(hour, 31));
                route.addStop(locationMap.get("Utrecht"), LocalTime.of(hour + 1, 0), LocalTime.of(hour + 1, 1));
                route.addEnd(locationMap.get("Rotterdam"), LocalTime.of(hour + 2, 0));
                addRoute(route);
            }
        }

        // Utrecht – Apeldoorn
        {
            for (int hour = 7; hour <= 22; hour += 1) {
                Route route = new Route(locationMap.get("Utrecht"), LocalTime.of(hour, 0));
                route.addEnd(locationMap.get("Apeldoorn"), LocalTime.of(hour, 30));
                addRoute(route);
            }
        }

        // Apeldoorn - Utrecht
        {
            for (int hour = 7; hour <= 22; hour += 1) {
                Route route = new Route(locationMap.get("Apeldoorn"), LocalTime.of(hour, 0));
                route.addEnd(locationMap.get("Utrecht"), LocalTime.of(hour, 30));
                addRoute(route);
            }
        }


    }

    private void addRoute(Route route) {
        var routeKey = route.getKey();
        var set = routeMap.get(routeKey);

        // If the key never occurred earlier, set will be null.
        // Create a fresh entry for this key.
        if (null == set) {
            set = new HashSet<Route>();
        }
        set.add(route);
        routeMap.put(routeKey, set);
    }

    public void printRoutes() {
        int count = 0;
        for (var e : routeMap.entrySet()) {
            System.out.format("route: %s\n", e.getKey());
            var set = e.getValue();
            for (var r : set) {
                System.out.format("%3d. %s: %s-%s\n", ++count, r.getKey(), r.getDeparture(), r.getArrival());
            }
        }
    }


    public HashMap getLoginInfo() {
        return logininfo;
    }

    public void addCredential(String newEmail,String password){
     logininfo.put(newEmail,password);
    }



    public void addUserJson(String newFirstName, String newSurName, String newAddress, String newEmail, String newPassword, ArrayList favoriteRoutes) {


        JsonParser parser = new JsonParser();

        try {
            JsonObject fileUser = (JsonObject) parser.parse(new FileReader("users.json")); // Read file
            JsonArray userArray = (JsonArray) fileUser.get("users"); // Create UserArray from object "User"

            JsonArray newList = new JsonArray();

            for (int i = 0; i < userArray.size(); i++) {

                JsonObject object = (JsonObject) userArray.get(i);

                String firstName = object.get("firstName").toString().replaceAll("\"", "");
                String surName = object.get("surName").toString().replaceAll("\"", "");
                String address = object.get("address").toString().replaceAll("\"", "");
                String email = object.get("email").toString().replaceAll("\"", "");
                String password = object.get("password").toString().replaceAll("\"", "");
                String list = object.get("favouriteRoutes").toString().replaceAll("\"", "");

                newList.add(object);
            }

            JsonObject newUser = new JsonObject();
            newUser.addProperty("firstName", newFirstName);
            newUser.addProperty("surName", newSurName);
            newUser.addProperty("address", newAddress);
            newUser.addProperty("email", newEmail);
            newUser.addProperty("password", newPassword);
            newUser.addProperty("favouriteRoutes", favoriteRoutes.toString());
            newList.add(newUser);

            JsonObject obj = new JsonObject();
            obj.add("users", newList.getAsJsonArray());

            FileWriter fileWriter = new FileWriter("users.json");
            fileWriter.write(obj.toString());
            fileWriter.flush();
            fileWriter.close();

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error: users.json not found (add user)");

        }
    }

    public void readJson() {

        Gson gson = new Gson();

        try {
            // Create JSON Parser, then get the file
            JsonParser parser = new JsonParser();
            JsonObject obj = (JsonObject) parser.parse(new FileReader("users.json"));

            // Create JsonArray from object "users"
            JsonArray jsonArray = (JsonArray) obj.get("users");

            ArrayList<User> Users = new ArrayList();


            for (int i = 0; i < jsonArray.size(); i++) {

                // Create Json object for every item in the JsonArray
                JsonObject object = (JsonObject) jsonArray.get(i);

                // Get values from object
                String firstName = object.get("firstName").toString();
                String surName = object.get("surName").toString();
                String address = object.get("address").toString();
                String email = object.get("email").toString();
                String password = object.get("password").toString();
                JsonArray favouriteRoutes = object.getAsJsonArray("favouriteRoutes");

                ArrayList jsonObjList = gson.fromJson(favouriteRoutes, ArrayList.class);

                Users.add(new User(firstName, surName, address, email, password, jsonObjList));
                Users.get(i).printUser();
                System.out.println("\n");

            }
        } catch (Exception e) {
            System.out.println("Error: users.json not found (readJson)");
        }
    }

    public SortedMap searchRoutes(String keyA, String keyB, LocalTime time) {
        int count = 0;

        SortedMap routeList = new TreeMap();
        String a = null;
        String b = null;
        String c = null;
        String d = null;
        String start = null;
        String end;

        for (var e : routeMap.entrySet()) {
            var routeKey = e.getKey();
            var posA = routeKey.indexOf(keyA);       //    indexOf("Utrecht") = plek van utrecht in de string (integer)
            if (posA >= 0) {
                var posB = routeKey.indexOf(keyB);
                if (posB > posA)                          //  posB > posA, dan   A--->B
                {
                    var set = e.getValue();
                    for (var r : set) {
                        System.out.format("%2d. %s: Departure %s Arrival %s\n", ++count, r.getKey(), r.getDeparture(), r.getArrival());
                        for (var p : r.stops) {
                            if (time.isBefore(p.getDeparture()) && p.getName().equalsIgnoreCase(keyA)) {
                                a = p.getName();
                                c = p.getDeparture().toString();
                            }
                            if (p.getName().equalsIgnoreCase(keyB) && p.getArrival().isAfter(p.getDeparture()) ) {
                                b = p.getName();
//                                d = p.getArrival().toString();
                            }
                            if (a != null && b != null && c != null ) {
                                start = c + a;
                                end =  b;
                                routeList.put(start, end);
                            }
                        }
                    }
                }
            }
        }
        return routeList;
    }
}


