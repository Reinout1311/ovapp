import org.junit.jupiter.api.Test;

import java.time.LocalTime;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class DataTest {

    Data data = new Data();

    @Test
    void getLoginInfo() {
        data.getLoginInfo();
    }

    @Test
    void addUserJson() {
       data.addUserJson("test","test2","test3","test4","test5",new ArrayList());
    }

    @Test
    void readJson() {
        data.readJson();

    }

    @Test
    void searchRoutes() {
       var a = data.searchRoutes("Groningen","Rotterdam", LocalTime.of(8,00));
        System.out.println(a);
    }
}