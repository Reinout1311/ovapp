import java.util.ArrayList;

public class User {

    private  String firstName;
    private  String surName;
    private   String address;
    private   String email;
    private   String password;
    private   ArrayList<Route> favoriteRoutes = new ArrayList();

    public User(String firstName,String surName,String address,String email, String password,ArrayList<Route> favoriteRoutes) {

        this.firstName = firstName;
        this.surName = surName;
        this.address = address;
        this.email = email;
        this.password = password;
        this.favoriteRoutes = favoriteRoutes;
    }

    public void printUser() {
        System.out.println("Name:      " + firstName + " " + surName);
        System.out.println("Address:   " + address);
        System.out.println("E-mail:    " + email);
        System.out.println("Password:  " + password);
        System.out.println("Favourite routes:" + favoriteRoutes);
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
