public class Location {


    private final String name;
    

    public Location(String name){

        this.name = name;

    }

    public void printLocation() {

        System.out.println(name);

    }

    public String getName() { return name; }

}