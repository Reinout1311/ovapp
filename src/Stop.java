import java.time.LocalTime;

 public class Stop extends Location {

    private LocalTime arrival;
    private LocalTime departure;

    public Stop(String name, LocalTime arrival, LocalTime departure){

        super(name);
        this.arrival = arrival;
        this.departure = departure;

    }

    public LocalTime getArrival() { return arrival; }

    public LocalTime getDeparture() { return departure; }

    @Override
    public java.lang.String getName() {
        return super.getName();
    }
}
